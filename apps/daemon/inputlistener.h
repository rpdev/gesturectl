#ifndef INPUTLISTENER_H
#define INPUTLISTENER_H

#include <complex>

#include <QHash>
#include <QObject>


class QThread;
class InputListenerRunner;


class InputListener : public QObject
{
    Q_OBJECT
public:
    explicit InputListener(QObject *parent = 0);
    virtual ~InputListener();

    bool isValid() const;
    void quit();

signals:

    /**
     * @brief The listener finished running.
     */
    void finished();

    /**
     * @brief A pinch gesture has been started.
     *
     * This signal is emitted to indicate that a pinch gesture with a certain amount
     * of @p fingers has been started. The gesture is identified by a @p id.
     */
    void pinchStarted(uint id, int fingers);

    /**
     * @brief The user pinched in.
     *
     * This signal indicates, that the user pinched in. The gesture is identified
     * by its @p id. It is comprised of the given number of @p fingers. The current
     * @p scale is reported as well. The scale starts at a value of 1.0 when the gesture
     * starts and changes accordingly depending on whether the user pinched in (scale
     * decreases) or out (scale increases).
     */
    void pinchIn(uint id, int fingers, double scale);

    /**
     * @brief The user pinched out.
     *
     * This signal indicates, that the user pinched out. The gesture is identified
     * by its @p id. It is comprised of the given number of @p fingers. The current
     * @p scale is reported as well. The scale starts at a value of 1.0 when the gesture
     * starts and changes accordingly depending on whether the user pinched in (scale
     * decreases) or out (scale increases).
     */
    void pinchOut(uint id, int fingers, double scale);

    /**
     * @brief A pinch gesture has ended.
     *
     * This signal is emitted to indicate that a pinch gesture has finished. The gesture
     * is identified by an @p id and is comprised of the given number of @p fingers.
     */
    void pinchEnded(uint id, int fingers);

    /**
     * @brief A swipe gesture has started.
     *
     * The swipe is identified by the @p id and the given @p number of fingers
     * are involved.
     */
    void swipeStarted(uint id, int fingers);

    /**
     * @brief A swipe continues.
     *
     * This signal is emitted when a swipe continues. It carries the unique
     * @p id and the number of fingers involved. Additionally, the relative
     * movement is communicated via the @p dx and @p dy arguments.
     */
    void swipeUpdate(uint id, int fingers, double dx, double dy);

    /**
     * @brief A swipe ended.
     *
     * This signal is emitted to indicate that he swipe identified by the @p id
     * and with the given @p number of fingers involved has finished.
     */
    void swipeEnded(uint id, int fingers);

    /**
     * @brief The user swiped up.
     *
     * This signal is emitted once when a swipe gesture ends. It carries the pinch
     * @p id and the number of @p fingers. The signal indicates that the swipe was
     * a swipe up gesture.
     */
    void swipeUp(uint id, int fingers);

    /**
     * @brief The user swiped up.
     *
     * This signal is emitted once when a swipe gesture ends. It carries the pinch
     * @p id and the number of @p fingers. The signal indicates that the swipe was
     * a swipe down gesture.
     */
    void swipeDown(uint id, int fingers);

    /**
     * @brief The user swiped up.
     *
     * This signal is emitted once when a swipe gesture ends. It carries the pinch
     * @p id and the number of @p fingers. The signal indicates that the swipe was
     * a swipe left gesture.
     */
    void swipeLeft(uint id, int fingers);

    /**
     * @brief The user swiped up.
     *
     * This signal is emitted once when a swipe gesture ends. It carries the pinch
     * @p id and the number of @p fingers. The signal indicates that the swipe was
     * a swipe right gesture.
     */
    void swipeRight(uint id, int fingers);

private:

    friend class InputListenerRunner;

    QThread*                m_thread;
    InputListenerRunner*    m_runner;

};


/**
 * @brief Helper class running the libinput event processing.
 *
 * This class is used to run the libinput event processing loop
 * in a background thread.
 */
class InputListenerRunner : public QObject
{
    Q_OBJECT

    friend class InputListener;

    struct context;

    const double MIN_PINCH_SCALE_DELTA  = 0.1;
    const double MIN_SWIPE_DISTANCE     = 100.0;

    InputListener*                      m_listener;
    context*                            m_context;
    QHash<uint, double>                 m_pinchScale;
    QHash<uint, std::complex<double>>   m_swipeDistance;
    bool                                m_quit;

    InputListenerRunner(InputListener* listener);
    virtual ~InputListenerRunner();

signals:

    void finished();
    void pinchStarted(uint id, int fingers);
    void pinchIn(uint id, int fingers, double scale);
    void pinchOut(uint id, int fingers, double scale);
    void pinchEnded(uint id, int fingers);
    void swipeStarted(uint id, int fingers);
    void swipeUpdate(uint id, int fingers, double dx, double dy);
    void swipeEnded(uint id, int fingers);
    void swipeUp(uint id, int fingers);
    void swipeDown(uint id, int fingers);
    void swipeLeft(uint id, int fingers);
    void swipeRight(uint id, int fingers);

private slots:
    void run();
    void handleEvents();
};


#endif // INPUTLISTENER_H
