#include "inputlistener.h"

#include <QDebug>
#include <QLoggingCategory>
#include <QThread>
#include <QTimer>

#include <libinput.h>
#include <libudev.h>
#include <fcntl.h>
#include <unistd.h>
#include <poll.h>

#include <cmath>


Q_DECLARE_LOGGING_CATEGORY(inputListenerLog)
Q_LOGGING_CATEGORY(inputListenerLog, "gesturectl.daemon.inputListener")


static const unsigned int WAIT_FOR_THREAD_IN_MS = 10 * 1000;


/**
 * @brief Holds context information for a libinput connection.
 */
struct InputListenerRunner::context {
    libinput*           li_handle    = nullptr;
    libinput_interface  li_interface;
    bool                valid        = false;
};


/**
 * @brief Open callback for libinput connections.
 * @param path
 * @param flags
 * @param user_data
 * @return
 */
static int open_restricted(const char *path, int flags, void *user_data)
{
    qCDebug(inputListenerLog).noquote() << "Opening device" << path
                                        << "with flags" << QString::number(flags, 16);
    (void) user_data;
    return open(path, flags);
}


/**
 * @brief Close callback for libinput connections.
 * @param fd
 * @param user_data
 */
static void close_restricted(int fd, void *user_data)
{
    (void) user_data;
    close(fd);
}


/**
 * @brief Compute an ID for the event.
 *
 * This helper function is used to get an ID for a given @p event. The ID
 * is used to identify gestures across their lifetime.
 */
static uint eventId(libinput_event* event) {
    auto device = libinput_event_get_device(event);
    auto gesture = libinput_event_get_gesture_event(event);
    QString sysname = libinput_device_get_sysname(device);
    int fingers = libinput_event_gesture_get_finger_count(gesture);
    return qHash(sysname) ^ qHash(fingers);
}


/**
 * @brief Constructor.
 *
 * Creates a new InputListened which is a child of the given @p parent.
 */
InputListener::InputListener(QObject *parent) :
    QObject(parent),
    m_thread(new QThread(this)),
    m_runner(new InputListenerRunner(this))
{
    m_thread->start();
    m_runner->moveToThread(m_thread);
    connect(m_runner, &InputListenerRunner::finished,
            this, &InputListener::finished);
    connect(m_runner, &InputListenerRunner::pinchStarted,
            this, &InputListener::pinchStarted);
    connect(m_runner, &InputListenerRunner::pinchIn,
            this, &InputListener::pinchIn);
    connect(m_runner, &InputListenerRunner::pinchOut,
            this, &InputListener::pinchOut);
    connect(m_runner, &InputListenerRunner::pinchEnded,
            this, &InputListener::pinchEnded);
    connect(m_runner, &InputListenerRunner::swipeStarted,
            this, &InputListener::swipeStarted);
    connect(m_runner, &InputListenerRunner::swipeUpdate,
            this, &InputListener::swipeUpdate);
    connect(m_runner, &InputListenerRunner::swipeEnded,
            this, &InputListener::swipeEnded);
    connect(m_runner, &InputListenerRunner::swipeUp,
            this, &InputListener::swipeUp);
    connect(m_runner, &InputListenerRunner::swipeDown,
            this, &InputListener::swipeDown);
    connect(m_runner, &InputListenerRunner::swipeLeft,
            this, &InputListener::swipeLeft);
    connect(m_runner, &InputListenerRunner::swipeRight,
            this, &InputListener::swipeRight);
    QTimer::singleShot(0, m_runner, &InputListenerRunner::run);
}


/**
 * @brief Destructor.
 */
InputListener::~InputListener()
{
    qCDebug(inputListenerLog) << "Destroying";
    m_thread->quit();
    m_thread->wait(WAIT_FOR_THREAD_IN_MS);
    if (m_thread->isRunning()) {
        m_thread->terminate();
        m_thread->wait(WAIT_FOR_THREAD_IN_MS);
    }
    delete m_runner;
}


/**
 * @brief Return true if the listener is valid.
 *
 * A listener is valid if it has a properly setup connection to
 * libinput.
 */
bool InputListener::isValid() const
{
    return m_runner->m_context->valid;
}


/**
 * @brief Request the listener to stop its event loop.
 */
void InputListener::quit()
{
    qCDebug(inputListenerLog) << "Quit requested";
    m_runner->m_quit = true;
}


/**
 * @brief Constructor.
 *
 * Creates a new runner object working on the given @p listener. Note that
 * the runner will not be a child of the listener, as it is intended
 * to be moved to another thread by it.
 */
InputListenerRunner::InputListenerRunner(InputListener* listener) :
    QObject(),
    m_listener(listener),
    m_context(new context()),
    m_pinchScale(),
    m_swipeDistance(),
    m_quit(false)
{
    udev* udev_handle = udev_new();
    if (udev_handle != nullptr) {
        m_context->li_interface.open_restricted = &open_restricted;
        m_context->li_interface.close_restricted = &close_restricted;
        m_context->li_handle = libinput_udev_create_context(
                    &m_context->li_interface, nullptr, udev_handle);
        if (m_context->li_handle != nullptr) {
            if (libinput_udev_assign_seat(m_context->li_handle, "seat0") == 0) {
                m_context->valid = true;
            } else {
                libinput_unref(m_context->li_handle);
                m_context->li_handle = nullptr;
            }
        }
        udev_unref(udev_handle);
    }
}

/**
 * @brief Destructor.
 */
InputListenerRunner::~InputListenerRunner()
{
    if (m_context->li_handle != nullptr) {
        libinput_unref(m_context->li_handle);
    }
    delete m_context;
}

/**
 * @brief Run the listener's event loop.
 */
void InputListenerRunner::run()
{
    qCDebug(inputListenerLog) << "Starting event reading loop";
    if (m_context->li_handle == nullptr) {
        qCWarning(inputListenerLog) << "Connection to libinput not up!";
    } else {
        m_quit = false;

        pollfd pfd;
        pfd.fd = libinput_get_fd(m_context->li_handle);
        pfd.events = POLLIN;
        pfd.revents = 0;

        handleEvents(); // Handle pending events
        while (!m_quit && poll(&pfd, 1, 1000) > -1) {
            handleEvents();
        }
    }
    emit finished();
}


/**
 * @brief Handle available events.
 */
void InputListenerRunner::handleEvents()
{
    libinput_event *event;
    libinput_dispatch(m_context->li_handle);
    libinput_event_gesture *ge = nullptr;

    while((event = libinput_get_event(m_context->li_handle))) {
        switch (libinput_event_get_type(event)) {
        case LIBINPUT_EVENT_GESTURE_PINCH_BEGIN:
        {
            ge = libinput_event_get_gesture_event(event);
            auto id = eventId(event);
            auto fingers = libinput_event_gesture_get_finger_count(ge);
            m_pinchScale.insert(id, 1.0);
            emit pinchStarted(id, fingers);
            break;
        }
        case LIBINPUT_EVENT_GESTURE_PINCH_UPDATE:
        {
            auto id = eventId(event);
            ge = libinput_event_get_gesture_event(event);
            auto scale = libinput_event_gesture_get_scale(ge);
            auto fingers = libinput_event_gesture_get_finger_count(ge);
            auto previousScale = m_pinchScale.value(id);
            auto deltaScale = scale - previousScale;
            if (deltaScale < -MIN_PINCH_SCALE_DELTA) {
                emit pinchIn(id, fingers, scale);
                m_pinchScale[id] = scale;
            } else if ( deltaScale > MIN_PINCH_SCALE_DELTA) {
                emit pinchOut(id, fingers, scale);
                m_pinchScale[id] = scale;
            }
            break;
        }
        case LIBINPUT_EVENT_GESTURE_PINCH_END:
        {
            ge = libinput_event_get_gesture_event(event);
            auto id = eventId(event);
            m_pinchScale.remove(id);
            auto fingers = libinput_event_gesture_get_finger_count(ge);
            emit pinchEnded(id, fingers);
            break;
        }
        case LIBINPUT_EVENT_GESTURE_SWIPE_BEGIN:
        {
            ge = libinput_event_get_gesture_event(event);
            auto id = eventId(event);
            auto fingers = libinput_event_gesture_get_finger_count(ge);
            emit swipeStarted(id, fingers);
            m_swipeDistance.insert(id, std::complex<double>(0.0, 0.0));
            break;
        }
        case LIBINPUT_EVENT_GESTURE_SWIPE_UPDATE:
        {
            ge = libinput_event_get_gesture_event(event);
            auto id = eventId(event);
            auto fingers = libinput_event_gesture_get_finger_count(ge);
            auto dx = libinput_event_gesture_get_dx(ge);
            auto dy = libinput_event_gesture_get_dy(ge);
            m_swipeDistance[id] += std::complex<double>(dx, dy);
            emit swipeUpdate(id, fingers, dx, dy);
            break;
        }
        case LIBINPUT_EVENT_GESTURE_SWIPE_END:
        {
            ge = libinput_event_get_gesture_event(event);
            auto id = eventId(event);
            auto fingers = libinput_event_gesture_get_finger_count(ge);
            emit swipeEnded(id, fingers);
            auto totalDistance = m_swipeDistance[id];
            auto dist = std::abs(totalDistance);
            if (dist >= MIN_SWIPE_DISTANCE) {
                auto angle = std::arg(totalDistance);
                if (angle >= -M_PI/4 && angle < M_PI/4) {
                    emit swipeRight(id, fingers);
                } else if (angle >= M_PI/4 && angle < 3*M_PI/4) {
                    emit swipeDown(id, fingers);
                } else if ((angle >= 3*M_PI/4 && angle <= M_PI) ||
                           (angle >= -M_PI && angle < -3*M_PI/4)) {
                    emit swipeLeft(id, fingers);
                } else if (angle >= -3*M_PI/4 && angle < -M_PI/4) {
                    emit swipeUp(id, fingers);
                } else {
                    qCWarning(inputListenerLog) << "Bad angle" << angle;
                }
            }
            m_swipeDistance.remove(id);
            break;
        }
        default:
            break;
        }
    }
}
