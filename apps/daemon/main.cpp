#include <QCoreApplication>
#include <QDBusConnection>

#include <signal.h>
#include <unistd.h>
#include <sys/types.h>

#include "inputlistener.h"
#include "gesturectladaptor.h"


static InputListener* g_listener;


static void handleSigInt(int signal) {
    (void) signal;
    if (g_listener != nullptr) {
        g_listener->quit();
    }
}


int main(int argc, char *argv[])
{
    setreuid(geteuid(), geteuid());
    setregid(getegid(), getegid());

    QCoreApplication a(argc, argv);

    InputListener listener;

    g_listener = &listener;
    signal(SIGINT, handleSigInt);

    if (g_listener->isValid()) {
        QObject::connect(&listener, &InputListener::finished,
                         &a, QCoreApplication::quit);
        new GesturectlAdaptor(g_listener);
        QDBusConnection::sessionBus().registerObject("/Gestures", g_listener);
        QDBusConnection::sessionBus().registerService("net.rpdev.gesturectl");
        return a.exec();
    } else {
        return 1;
    }
}
