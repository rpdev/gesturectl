#!/bin/sh
if [ -f $1 ]; then
    chgrp input $1
    if [ $? == 0 ]; then
        chmod 2755 $1
    fi
fi
