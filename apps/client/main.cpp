#include "gesturectlinterface.h"
#include "gestures.h"
#include "pinchbegingesture.h"
#include "pinchendgesture.h"
#include "pinchingesture.h"
#include "pinchoutgesture.h"
#include "swipebegingesture.h"
#include "swipeupdategesture.h"
#include "swipeendgesture.h"
#include "swipeupgesture.h"
#include "swipedowngesture.h"
#include "swipeleftgesture.h"
#include "swiperightgesture.h"

#include <QCoreApplication>
#include <QSharedPointer>
#include <QStandardPaths>


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QString configFile = QStandardPaths::locate(
                QStandardPaths::ConfigLocation, Gestures::DefaultConfigFileName);
    if (configFile.isEmpty()) {
        configFile = QStandardPaths::writableLocation(
                    QStandardPaths::ConfigLocation) +
                "/" + Gestures::DefaultConfigFileName;
    }
    auto gestures = new Gestures(configFile, &a);
    QDBusConnection::sessionBus().interface()->startService("net.rpdev.gesturectl");
    auto interface = new net::rpdev::gesturectl(
                "net.rpdev.gesturectl", "/Gestures",
                QDBusConnection::sessionBus(), &a);

    // Dispatch signals into gestures:
    QObject::connect(interface, &net::rpdev::gesturectl::pinchStarted,
                     [gestures](uint id, int fingers) {
        auto gesture = new PinchBeginGesture();
        gesture->setId(id);
        gesture->setFingers(fingers);
        gestures->process(QSharedPointer<const Gesture>(gesture));
    });
    QObject::connect(interface, &net::rpdev::gesturectl::pinchIn,
                     [gestures](uint id, int fingers, double scale) {
        auto gesture = new PinchInGesture();
        gesture->setId(id);
        gesture->setFingers(fingers);
        gesture->setScale(scale);
        gestures->process(QSharedPointer<const Gesture>(gesture));
    });
    QObject::connect(interface, &net::rpdev::gesturectl::pinchOut,
                     [gestures](uint id, int fingers, double scale) {
        auto gesture = new PinchOutGesture();
        gesture->setId(id);
        gesture->setFingers(fingers);
        gesture->setScale(scale);
        gestures->process(QSharedPointer<const Gesture>(gesture));
    });
    QObject::connect(interface, &net::rpdev::gesturectl::pinchEnded,
                     [gestures](uint id, int fingers) {
        auto gesture = new PinchEndGesture();
        gesture->setId(id);
        gesture->setFingers(fingers);
        gestures->process(QSharedPointer<const Gesture>(gesture));
    });
    QObject::connect(interface, &net::rpdev::gesturectl::swipeStarted,
                     [gestures](uint id, int fingers) {
        auto gesture = new SwipeBeginGesture();
        gesture->setId(id);
        gesture->setFingers(fingers);
        gestures->process(QSharedPointer<const Gesture>(gesture));
    });
    QObject::connect(interface, &net::rpdev::gesturectl::swipeUpdate,
                     [gestures](uint id, int fingers, double dx, double dy) {
        auto gesture = new SwipeUpdateGesture();
        gesture->setId(id);
        gesture->setFingers(fingers);
        gesture->setDx(dx);
        gesture->setDy(dy);
        gestures->process(QSharedPointer<const Gesture>(gesture));
    });
    QObject::connect(interface, &net::rpdev::gesturectl::swipeEnded,
                     [gestures](uint id, int fingers) {
        auto gesture = new SwipeEndGesture();
        gesture->setId(id);
        gesture->setFingers(fingers);
        gestures->process(QSharedPointer<const Gesture>(gesture));
    });
    QObject::connect(interface, &net::rpdev::gesturectl::swipeUp,
                     [gestures](uint id, int fingers) {
        auto gesture = new SwipeUpGesture();
        gesture->setId(id);
        gesture->setFingers(fingers);
        gestures->process(QSharedPointer<const Gesture>(gesture));
    });
    QObject::connect(interface, &net::rpdev::gesturectl::swipeDown,
                     [gestures](uint id, int fingers) {
        auto gesture = new SwipeDownGesture();
        gesture->setId(id);
        gesture->setFingers(fingers);
        gestures->process(QSharedPointer<const Gesture>(gesture));
    });
    QObject::connect(interface, &net::rpdev::gesturectl::swipeLeft,
                     [gestures](uint id, int fingers) {
        auto gesture = new SwipeLeftGesture();
        gesture->setId(id);
        gesture->setFingers(fingers);
        gestures->process(QSharedPointer<const Gesture>(gesture));
    });
    QObject::connect(interface, &net::rpdev::gesturectl::swipeRight,
                     [gestures](uint id, int fingers) {
        auto gesture = new SwipeRightGesture();
        gesture->setId(id);
        gesture->setFingers(fingers);
        gestures->process(QSharedPointer<const Gesture>(gesture));
    });

    return a.exec();
}
