#include "gesture.h"

Gesture::Gesture(QObject *parent) : QObject(parent),
    m_id(0),
    m_fingers(-1)
{

}

uint Gesture::id() const
{
    return m_id;
}

void Gesture::setId(const uint& id)
{
    if (m_id != id) {
        m_id = id;
        emit idChanged();
    }
}

int Gesture::fingers() const
{
    return m_fingers;
}

void Gesture::setFingers(int fingers)
{
    if (m_fingers != fingers) {
        m_fingers = fingers;
        emit fingersChanged();
    }
}
