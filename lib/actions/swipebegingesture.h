#ifndef SWIPEBEGINGESTURE_H
#define SWIPEBEGINGESTURE_H

#include "gesture.h"

#include <QObject>

class SwipeBeginGesture : public Gesture
{
    Q_OBJECT
public:
    explicit SwipeBeginGesture(QObject *parent = 0);

signals:

public slots:
};

#endif // SWIPEBEGINGESTURE_H
