#ifndef GESTURES_H
#define GESTURES_H

#include <QList>
#include <QObject>
#include <QSharedPointer>
#include <QVariant>


// Forward declarations:
class Action;
class Gesture;
class QFileSystemWatcher;


/**
 * @brief Gesture library.
 *
 * This class is used to hold actions bound to gestures. It provides the appropriate interfaces
 * for loading such actions from a configuration file and also write such a file.
 */
class Gestures : public QObject
{
    Q_OBJECT

public:
    static const char* DefaultConfigFileName;

    explicit Gestures(const QString& configFile, QObject* parent = nullptr);
    explicit Gestures(const QVariant& config, QObject* parent = nullptr);
    explicit Gestures(QObject* parent = nullptr);

    const QList<QSharedPointer<Action>>& actions() const { return m_actions; }
    void addAction(Action* action);

    QVariant config() const;
    void setConfig(const QVariant& config);

    void process(QSharedPointer<const Gesture> gesture);

signals:

public slots:

private:

    QFileSystemWatcher*             m_watcher;
    QString                         m_configFile;
    QVariant                        m_config;
    QList<QSharedPointer<Action>>   m_actions;

    void serialize();
    void deserialize();
    static Action* createAction(const QByteArray& type);

private slots:
    void load();
};

#endif // GESTURES_H
