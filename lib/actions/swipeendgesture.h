#ifndef SWIPEENDGESTURE_H
#define SWIPEENDGESTURE_H

#include "gesture.h"

#include <QObject>

class SwipeEndGesture : public Gesture
{
    Q_OBJECT
public:
    explicit SwipeEndGesture(QObject *parent = 0);

signals:

public slots:
};

#endif // SWIPEENDGESTURE_H
