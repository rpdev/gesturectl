#ifndef SWIPEDOWNGESTURE_H
#define SWIPEDOWNGESTURE_H

#include "gesture.h"

#include <QObject>

class SwipeDownGesture : public Gesture
{
    Q_OBJECT
public:
    explicit SwipeDownGesture(QObject *parent = 0);

signals:

public slots:
};

#endif // SWIPEDOWNGESTURE_H
