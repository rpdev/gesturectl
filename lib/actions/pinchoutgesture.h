#ifndef PINCHOUTGESTURE_H
#define PINCHOUTGESTURE_H

#include <QObject>

#include "gesture.h"


class PinchOutGesture : public Gesture
{
    Q_OBJECT

    Q_PROPERTY(double scale READ scale WRITE setScale NOTIFY scaleChanged)
public:
    explicit PinchOutGesture(QObject *parent = 0);

    double scale() const;
    void setScale(double scale);

signals:

    void scaleChanged();

public slots:

private:

    double m_scale;
};

#endif // PINCHOUTGESTURE_H
