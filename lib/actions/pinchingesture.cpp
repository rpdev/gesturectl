#include "pinchingesture.h"

PinchInGesture::PinchInGesture(QObject *parent) : Gesture(parent),
    m_scale(1.0)
{

}

double PinchInGesture::scale() const
{
    return m_scale;
}

void PinchInGesture::setScale(double scale)
{
    if (!qFuzzyCompare(m_scale, scale)) {
        m_scale = scale;
        emit scaleChanged();
    }
}
