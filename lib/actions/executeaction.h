#ifndef EXECUTEACTION_H
#define EXECUTEACTION_H

#include "action.h"

#include <QObject>


/**
 * @brief Execute programs.
 *
 * This Action class allows to execute applications in response to a gesture.
 */
class ExecuteAction : public Action
{
    Q_OBJECT

    Q_PROPERTY(QByteArray command READ command WRITE setCommand NOTIFY commandChanged)
public:
    explicit ExecuteAction(QObject *parent = 0);

    QByteArray command() const;
    void setCommand(const QByteArray& command);


    // Action interface
    void execute(QSharedPointer<const Gesture> gesture) override;
    QVariantMap saveSettings() const override;
    void restoreSettings(const QVariantMap& settings) override;

signals:

    void commandChanged();

public slots:

private:

    QByteArray m_command;

};

#endif // EXECUTEACTION_H
