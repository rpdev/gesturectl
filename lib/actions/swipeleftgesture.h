#ifndef SWIPELEFTGESTURE_H
#define SWIPELEFTGESTURE_H

#include "gesture.h"

#include <QObject>

class SwipeLeftGesture : public Gesture
{
    Q_OBJECT
public:
    explicit SwipeLeftGesture(QObject *parent = 0);

signals:

public slots:
};

#endif // SWIPELEFTGESTURE_H
