#include "swipeupdategesture.h"

SwipeUpdateGesture::SwipeUpdateGesture(QObject *parent) : Gesture(parent)
{

}

double SwipeUpdateGesture::dx() const
{
    return m_dx;
}

void SwipeUpdateGesture::setDx(double dx)
{
    if (!qFuzzyCompare(m_dx, dx)) {
        m_dx = dx;
        emit dxChanged();
    }
}

double SwipeUpdateGesture::dy() const
{
    return m_dy;
}

void SwipeUpdateGesture::setDy(double dy)
{
    if (!qFuzzyCompare(m_dy, dy)) {
        m_dy = dy;
        emit dyChanged();
    }
}
