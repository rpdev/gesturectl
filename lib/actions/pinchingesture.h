#ifndef PINCHINGESTURE_H
#define PINCHINGESTURE_H

#include <QObject>

#include "gesture.h"


class PinchInGesture : public Gesture
{
    Q_OBJECT

    Q_PROPERTY(double scale READ scale WRITE setScale NOTIFY scaleChanged)
public:
    explicit PinchInGesture(QObject *parent = 0);

    double scale() const;
    void setScale(double scale);

signals:

    void scaleChanged();

public slots:

private:

    double m_scale;

};

#endif // PINCHINGESTURE_H
