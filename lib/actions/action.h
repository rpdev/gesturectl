#ifndef ACTION_H
#define ACTION_H

#include <QLoggingCategory>
#include <QObject>
#include <QSharedPointer>
#include <QVariant>
#include <QVariantMap>


// Forward declarations:
class Gesture;


/**
 * @brief Base class for actions to bind gestures to.
 *
 * This class is the base for all actions a gesture can be bound to. Basically, the class defines
 * the execute() method, which receives the Gesture to be handled. Sub-classes are
 * supposed to translate the gesture arguments to actions, such as invoking keyboard shortcuts
 * are executing user defined applications.
 *
 * A particular instance of the Action class can define filters such as the name of a gesture
 * or the number of fingers involved in the gesture. The handlesGesture() method can be
 * used to test if the action handles a particular gesture.
 */
class Action : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QByteArray gesture READ gesture WRITE setGesture NOTIFY gestureChanged)
    Q_PROPERTY(int fingers READ fingers WRITE setFingers NOTIFY fingersChanged)
public:
    explicit Action(QObject* parent = nullptr);

    /**
     * @brief Executes the action.
     *
     * This method is called to execute an action bound to the given
     * @p gesture. Sub-classes are supposed to implement this method to actually
     * bind to user defined behavior, such as calling an external program or sending
     * keyboard shortcuts.
     */
    virtual void execute(QSharedPointer<const Gesture> gesture) = 0;

    virtual QVariantMap saveSettings() const;
    virtual void restoreSettings(const QVariantMap& settings);

    bool handlesGesture(QSharedPointer<const Gesture> gesture);

    QByteArray gesture() const;
    void setGesture(const QByteArray& gesture);

    int fingers() const;
    void setFingers(int fingers);

signals:

    void gestureChanged();
    void fingersChanged();

private:

    QByteArray  m_gesture;
    int         m_fingers;


};


Q_DECLARE_LOGGING_CATEGORY(action)


#endif // ACTION_H
