#ifndef PINCHENDGESTURE_H
#define PINCHENDGESTURE_H

#include <QObject>

#include "gesture.h"


class PinchEndGesture : public Gesture
{
    Q_OBJECT
public:
    explicit PinchEndGesture(QObject *parent = 0);

signals:

public slots:
};

#endif // PINCHENDGESTURE_H
