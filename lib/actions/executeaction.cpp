#include "executeaction.h"

#include "gesture.h"

#include <QDebug>
#include <QMetaObject>
#include <QMetaProperty>
#include <QProcess>


ExecuteAction::ExecuteAction(QObject *parent) : Action(parent),
    m_command(QByteArray())
{

}


/**
 * @brief The command to execute.
 *
 * This is the command that is executed when the action is invoked. The command
 * string is passed through as-is to the underlying shell. Hence, expansion of
 * environment variables is done by the shell of the user. In addition, placeholders of
 * the form '${propertyName}' are allowed, where 'propertyName' is the name of a
 * property of the gesture which caused the action to be executed. The placeholder will
 * be replaced by the value of the property.
 */
QByteArray ExecuteAction::command() const
{
    return m_command;
}


/**
 * @brief Set the @p command the action executes.
 */
void ExecuteAction::setCommand(const QByteArray& command)
{
    if (m_command != command) {
        m_command = command;
        emit commandChanged();
    }
}

void ExecuteAction::execute(QSharedPointer<const Gesture> gesture)
{
    if (gesture != nullptr && !m_command.isEmpty()) {
        QByteArray command = m_command;
        auto metaObject = gesture->metaObject();
        for (int i = 0; i < metaObject->propertyCount(); ++i) {
            auto property = metaObject->property(i);
            auto value = gesture->property(property.name());
            QByteArray placeholder = "${";
            placeholder += property.name();
            placeholder += "}";
            command = command.replace(placeholder, value.toByteArray());
        }
        qCDebug(action) << "Executing command" << command;
        QProcess::startDetached(command);
    }
}

QVariantMap ExecuteAction::saveSettings() const
{
    auto result = Action::saveSettings();
    result.insert("command", m_command);
    return result;
}

void ExecuteAction::restoreSettings(const QVariantMap& settings)
{
    Action::restoreSettings(settings);
    m_command = settings.value("command", QByteArray()).toByteArray();
}
