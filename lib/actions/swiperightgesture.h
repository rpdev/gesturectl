#ifndef SWIPERIGHTGESTURE_H
#define SWIPERIGHTGESTURE_H

#include "gesture.h"

#include <QObject>

class SwipeRightGesture : public Gesture
{
    Q_OBJECT
public:
    explicit SwipeRightGesture(QObject *parent = 0);

signals:

public slots:
};

#endif // SWIPERIGHTGESTURE_H
