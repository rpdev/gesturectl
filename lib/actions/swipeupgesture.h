#ifndef SWIPEUPGESTURE_H
#define SWIPEUPGESTURE_H

#include "gesture.h"

#include <QObject>

class SwipeUpGesture : public Gesture
{
    Q_OBJECT
public:
    explicit SwipeUpGesture(QObject *parent = 0);

signals:

public slots:
};

#endif // SWIPEUPGESTURE_H
