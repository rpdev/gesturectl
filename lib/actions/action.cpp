#include "action.h"

#include "gesture.h"

#include <QMetaObject>
#include <QRegExp>
#include <QVariantMap>


Q_LOGGING_CATEGORY(action, "gesturectl.client.action")


/**
 * @brief Constructor.
 */
Action::Action(QObject* parent) :
    QObject(parent),
    m_gesture("*"),
    m_fingers(-1)
{

}


/**
 * @brief Saves the settings of the action.
 *
 * This method is supposed to save the settings of the action to a QVariant. This is used
 * to save the action's settings in a configuration and restore it later using the
 * restoreSettings() method.
 */
QVariantMap Action::saveSettings() const
{
    QVariantMap result;
    result.insert("gesture", m_gesture);
    if (m_fingers >= 0) {
        result.insert( "fingers", m_fingers);
    }
    return result;
}


/**
 * @brief Restore the settings of the action.
 *
 * This method takes the @p settings previously generated
 * by calling the saveSettings() method and restores the settings.
 */
void Action::restoreSettings(const QVariantMap& settings)
{
    m_gesture = settings.value("gesture", "").toByteArray();
    m_fingers = settings.value("fingers", -1).toInt();
}


bool Action::handlesGesture(QSharedPointer<const Gesture> gesture)
{
    if (gesture != nullptr) {
        const char* GestureSuffix = "Gesture";
        QString className = gesture->metaObject()->className();
        className = className.mid(0, className.length() -
                                  static_cast<int>(sizeof(GestureSuffix) /
                                                   sizeof(GestureSuffix[0])) + 1);
        QRegExp re(m_gesture, Qt::CaseInsensitive);
        if (!re.exactMatch(className)) {
            return false;
        }

        if (m_fingers >= 0 && m_fingers != gesture->fingers()) {
            return false;
        }
        return true;
    }
    return false;
}


/**
 * @brief The name of the gesture the action is bound to.
 *
 * This property holds the name of the gesture the action is bound to. This
 * is supposed to be the full class name of sub-classes of the Gesture class with the
 * trailing "Gesture" string removed. Wild-cards are allowed as well; e.g. to handle
 * all gestures, the gesture name can be set to the string "*".
 * @return
 */
QByteArray Action::gesture() const
{
    return m_gesture;
}


/**
 * @brief Set the gesture property.
 */
void Action::setGesture(const QByteArray& gesture)
{
    if (m_gesture != gesture) {
        m_gesture = gesture;
        emit gestureChanged();
    }
}


/**
 * @brief The number of fingers the action handles.
 *
 * This property holds the required number of fingers involved in a gesture the
 * action is supposed to handle. This can be set to negative value to handle any
 * number of fingers.
 */
int Action::fingers() const
{
    return m_fingers;
}

/**
 * @brief Set the fingers property.
 */
void Action::setFingers(int fingers)
{
    if (m_fingers != fingers) {
        m_fingers = fingers;
        emit fingersChanged();
    }
}
