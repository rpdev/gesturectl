#include "pinchoutgesture.h"

PinchOutGesture::PinchOutGesture(QObject *parent) : Gesture(parent),
    m_scale(1.0)
{

}

double PinchOutGesture::scale() const
{
    return m_scale;
}

void PinchOutGesture::setScale(double scale)
{
    if (!qFuzzyCompare(m_scale, scale)) {
        m_scale = scale;
        emit scaleChanged();
    }
}
