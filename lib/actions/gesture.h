#ifndef GESTURE_H
#define GESTURE_H

#include <QObject>
#include <QByteArray>


/**
 * @brief Base class for gesture representations.
 *
 * This class is a base for all classes that are used to
 * represent gestures. For each individual gesture, a sub-class is
 * supposed to be available, holding the specific properties of each
 * gesture. An instance of such a sub-class is then passed into the
 * Action::execute() method.
 */
class Gesture : public QObject
{
    Q_OBJECT

    Q_PROPERTY(uint id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(int fingers READ fingers WRITE setFingers NOTIFY fingersChanged)

public:
    explicit Gesture(QObject *parent = 0);

    uint id() const;
    void setId(const uint& id);

    int fingers() const;
    void setFingers(int fingers);

signals:

    void idChanged();
    void fingersChanged();

public slots:

private:

    uint m_id;
    int  m_fingers;
};

#endif // GESTURE_H
