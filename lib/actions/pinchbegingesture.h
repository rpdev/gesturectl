#ifndef PINCHBEGINGESTURE_H
#define PINCHBEGINGESTURE_H

#include <QObject>

#include "gesture.h"


class PinchBeginGesture : public Gesture
{
    Q_OBJECT
public:
    explicit PinchBeginGesture(QObject *parent = 0);

signals:

public slots:
};

#endif // PINCHBEGINGESTURE_H
