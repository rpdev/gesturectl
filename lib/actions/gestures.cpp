#include "gestures.h"

#include "action.h"
#include "gesture.h"
#include "executeaction.h"

#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QFileSystemWatcher>
#include <QJsonDocument>
#include <QJsonParseError>
#include <QLoggingCategory>
#include <QMetaObject>
#include <QMetaObject>
#include <QTimer>
#include <QVariant>
#include <QVariantMap>


Q_DECLARE_LOGGING_CATEGORY(gestures)
Q_LOGGING_CATEGORY(gestures, "gesturectl.client.gestures")


const char* Gestures::DefaultConfigFileName = "gesturectl.json";


Gestures::Gestures(const QString& configFile, QObject* parent) :
    QObject(parent),
    m_watcher(new QFileSystemWatcher(this)),
    m_configFile(configFile),
    m_config(),
    m_actions()
{
    load();
    m_watcher->addPath(m_configFile);

    QFileInfo fi(m_configFile);
    m_watcher->addPath(fi.path());
    auto absoluteConfigPath = fi.absoluteFilePath();

    connect(m_watcher, &QFileSystemWatcher::fileChanged,
            [this, absoluteConfigPath](const QString& path) {
        if (QFileInfo(path).absoluteFilePath() == absoluteConfigPath) {
            // Re-load delayed, otherwise file might not be accessible.
            QTimer::singleShot(5000, this, &Gestures::load);
        }
    });
}


Gestures::Gestures(const QVariant& config, QObject* parent) :
    QObject(parent),
    m_watcher(nullptr),
    m_configFile(),
    m_config(config),
    m_actions()
{
}


Gestures::Gestures(QObject *parent) :
    QObject(parent),
    m_watcher(nullptr),
    m_configFile(),
    m_config(),
    m_actions()
{
}


/**
 * @brief Add an action.
 *
 * This method adds the @p action to the list of actions of the gesture
 * library. Note that the action is afterwards owned by the Gestured object
 * and must not be destroyed manually.
 */
void Gestures::addAction(Action* action)
{
    if (action != nullptr) {
        action->setParent(nullptr);
        m_actions.append(QSharedPointer<Action>(action));
        serialize();
    }
}


/**
 * @brief The configuration of the gesture library.
 *
 * This returns a serialized version of the actions in the gesture library.
 */
QVariant Gestures::config() const
{
    return m_config;
}

void Gestures::setConfig(const QVariant& config)
{
    if (m_config != config) {
        m_config = config;
        deserialize();
    }
}

void Gestures::process(QSharedPointer<const Gesture> gesture)
{
    qCDebug(gestures) << "Processing gesture" << gesture->metaObject()->className();
    for (auto action : m_actions) {
        if (action->handlesGesture(gesture)) {
            action->execute(gesture);
        }
    }
}

void Gestures::serialize()
{
    QVariantList actions;
    for (auto action : m_actions) {
        auto actionSettings = action->saveSettings();
        const char* ActionSuffix = "Action";
        auto metaObject = action->metaObject();
        QByteArray className = metaObject->className();
        if (className.endsWith(ActionSuffix)) {
            className = className.mid(0, className.length() -
                                      static_cast<int>(sizeof(ActionSuffix) /
                                                       sizeof(ActionSuffix[0])) + 1);
        }
        actionSettings.insert("type", className);
        actions << actionSettings;
    }
    QVariantMap config;
    config.insert("gestures", actions);
    m_config = config;
}

void Gestures::deserialize()
{
    auto actions = m_config.toMap().value("gestures").toList();
    m_actions.clear();
    for (auto entry : actions) {
        auto actionSettings = entry.toMap();
        auto action = createAction(actionSettings.value("type").toByteArray());
        if (action != nullptr) {
            action->restoreSettings(actionSettings);
            addAction(action);
        }
    }
}

Action*Gestures::createAction(const QByteArray& type)
{
    if (type == "Execute") {
        return new ExecuteAction();
    } else {
        qCWarning(gestures) << "Unknown action type" << type;
        return nullptr;
    }
}

void Gestures::load()
{
    qCDebug(gestures) << "Loading gestures from" << m_configFile;
    QFile file(m_configFile);
    if (file.open(QFile::ReadOnly)) {
        QJsonParseError error;
        QJsonDocument doc = QJsonDocument::fromJson(file.readAll(), &error);
        if (!doc.isNull()) {
            setConfig(doc.toVariant());
        } else {
            qCWarning(gestures) << "Failed to parse config file:" << error.errorString();
        }
        file.close();
    } else {
        qCWarning(gestures) << "Unable to open" << m_configFile << "for reading:"
                            << file.errorString();
    }
}
