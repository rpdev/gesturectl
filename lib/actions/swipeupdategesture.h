#ifndef SWIPEUPDATEGESTURE_H
#define SWIPEUPDATEGESTURE_H

#include "gesture.h"

#include <QObject>

class SwipeUpdateGesture : public Gesture
{
    Q_OBJECT

    Q_PROPERTY(double dx READ dx WRITE setDx NOTIFY dxChanged)
    Q_PROPERTY(double dy READ dy WRITE setDy NOTIFY dyChanged)
public:
    explicit SwipeUpdateGesture(QObject *parent = 0);

    double dx() const;
    void setDx(double dx);

    double dy() const;
    void setDy(double dy);

signals:

    void dxChanged();
    void dyChanged();

public slots:

private:

    double m_dx;
    double m_dy;
};

#endif // SWIPEUPDATEGESTURE_H
