Name:           gesturectl
Version:        0.1
Release:        0%{?dist}
Summary:        Map gestures from trackpads and touchscreens to commands
License:        GPL
URL:            https://www.rpdev.net/wordpress/apps/gesturectl/
Source0:        gesturectl-%{version}.tar.gz
BuildRequires:  make
BuildRequires:  cmake
BuildRequires:  qt5-qtbase-devel
BuildRequires:  libinput-devel
BuildRequires:  systemd-devel
Requires:       xdotool
Requires:       wmctrl
%description
Gesturectl is a utility which maps gestures recognized by libinput to
user provided commands. This can be used to map e.g. swipe left/right gestures
with three or more fingers to switching between virtual desktops, swiping up to
show an app expose and so on.

%global debug_package %{nil}
%prep
%autosetup

%build
%cmake -DCMAKE_SKIP_RPATH=TRUE .
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}

%files
%attr(2755, root, input) %{_bindir}/gestured
%{_bindir}/gesturectl
%{_datadir}/dbus-1/interfaces/net.rpdev.gesturectl.xml
%{_datadir}/dbus-1/services/net.rpdev.gesturectl.service
%{_sysconfdir}/xdg/autostart/gesturectl.desktop

%changelog
* Mon Jan 02 2017 Initial Release 0.1
-
